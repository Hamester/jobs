const CracoLessPlugin = require("craco-less");

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          modifyVars: {
            "@primary-color": "#900c3f",
            "@link-color": "#581845",
            "@border-radius-base": "2px",
          },
          javascriptEnabled: true,
        },
      },
    },
  ],
};
