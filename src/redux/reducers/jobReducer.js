import { JOB } from "../types";

export const initialState = {};

export default (state = initialState, { type, payload } = {}) => {
  switch (type) {
    case JOB.DATA:
      return {
        ...state,
        ...payload,
      };

    case JOB.REQUEST:
      return {
        ...state,
        request: {
          ...state.request,
          ...payload,
        },
      };

    case JOB.DASHBOARD:
      return {
        ...state,
        dashboard: {
          ...state.dashboard,
          ...payload,
        },
      };

    case JOB.ERROR:
      return {
        ...state,
        error: payload,
      };

    case JOB.CLEAR:
      return initialState;

    default:
      return state;
  }
};
