import React from "react";

import JobTable from "../../components/JobTable";
import JobModal from "../../components/JobModal";

import { Button, Row } from "antd";
import { useJob } from "../../hooks/useJob";

import "../../styles/main.less";
import "./App.less";

const App = () => {
  const { setJobData } = useJob();

  return (
    <div className="App">
      <JobModal />
      <Row type="flex" justify="space-between">
        <h1>Jobs</h1>

        <Button
          size="large"
          onClick={() => {
            setJobData({ displayRequest: true });
          }}
        >
          Add
        </Button>
      </Row>
      <Row>
        <JobTable />
      </Row>
    </div>
  );
};

export default App;
