import { useDispatch, useSelector } from "react-redux";
import { JOB } from "../redux/types";
import { message } from "antd";
import shortid from "shortid";

export const useJob = () => {
  const job = useSelector((store) => {
    const { error = null, dashboard = {}, request = {} } = store.job;

    return {
      ...store.job,
      error,
      dashboard,
      request,
    };
  });

  const dispatch = useDispatch();

  const setJobData = (values) => {
    dispatch({
      type: JOB.DATA,
      payload: values,
    });
  };

  const clearJobRequest = () => {
    dispatch({
      type: JOB.DATA,
      payload: { request: {} },
    });
  };

  const setJobRequest = (values) => {
    dispatch({
      type: JOB.REQUEST,
      payload: values,
    });
  };

  const setJobDashboard = (values) => {
    dispatch({
      type: JOB.DASHBOARD,
      payload: values,
    });
  };

  const onSaveJob = () => {
    const createdAt = new Date();

    const dataSource = [
      ...job.dashboard.dataSource,
      {
        id: shortid.generate(),
        createdAt: createdAt.toDateString(),
        ...job.request,
      },
    ];

    dispatch({
      type: JOB.DASHBOARD,
      payload: { dataSource },
    });

    clearJobRequest();
  };

  const onUpdateJob = ({ id, ...rest }) => {
    const dataSource = job.dashboard.dataSource.map((jobItem) => {
      if (jobItem.id === id) {
        return {
          ...jobItem,
          ...rest,
        };
      }

      return jobItem;
    });

    dispatch({
      type: JOB.DASHBOARD,
      payload: { dataSource },
    });
  };

  const onDeleteJob = (id) => {
    const itemFound = job.dashboard.dataSource.find(
      (jobItem) => jobItem.id === id
    );

    const index = job.dashboard.dataSource.indexOf(itemFound);
    if (index > -1) {
      job.dashboard.dataSource.splice(index, 1);
      dispatch({
        type: JOB.DASHBOARD,
        payload: { dataSource: job.dashboard.dataSource },
      });
    } else {
      message.info("Has been a problem deliting the item");
    }
    setJobData({
      displayRequest: false,
    });
    clearJobRequest();
  };

  return {
    job,
    setJobData,
    onSaveJob,
    clearJobRequest,
    setJobRequest,
    setJobDashboard,
    onUpdateJob,
    onDeleteJob,
  };
};
