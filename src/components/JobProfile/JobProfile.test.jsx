import React from 'react';
import ReactDOM from 'react-dom';
import JobProfile from './JobProfile';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<JobProfile />, div);
    ReactDOM.unmountComponentAtNode(div);
});
