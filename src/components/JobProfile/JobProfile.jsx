import { Button, Input, message, Row } from "antd";
import React, { Fragment, useEffect } from "react";
import { useJob } from "../../hooks/useJob";

import "./JobProfile.less";

const JobProfile = () => {
  const {
    onUpdateJob,
    onDeleteJob,
    onSaveJob,
    clearJobRequest,
    setJobRequest,
    job: {
      request,
      request: { id, jobTitle, jobDescription, companyName, applyUrl, edit },
    },
  } = useJob();

  useEffect(() => {
    return () => {
      clearJobRequest();
    };
  }, []);

  const onChangeHandler = (name, value) => {
    setJobRequest({ [name]: value });
  };

  return (
    <div className="JobProfile">
      <h2>{edit ? "Edit " : "New "} Job Profile</h2>

      <Row>
        <Input
          placeholder="Title"
          name="jobTitle"
          value={jobTitle}
          onChange={({ target: { value, name } }) => {
            onChangeHandler(name, value);
          }}
        />
      </Row>
      <Row>
        <Input.TextArea
          rows={5}
          placeholder="Description"
          name="jobDescription"
          value={jobDescription}
          onChange={({ target: { value, name } }) => {
            onChangeHandler(name, value);
          }}
        />
      </Row>
      <Row>
        <Input
          placeholder="Company name"
          name="companyName"
          value={companyName}
          onChange={({ target: { value, name } }) => {
            onChangeHandler(name, value);
          }}
        />
      </Row>
      <Row>
        <Input
          placeholder="Apply url"
          value={applyUrl}
          name="applyUrl"
          onChange={({ target: { value, name } }) => {
            onChangeHandler(name, value);
          }}
        />
      </Row>
      <Row type="flex" justify="end">
        {edit ? (
          <Fragment>
            <Button
              type="danger"
              size="large"
              onClick={() => {
                onDeleteJob(id);
                message.success("Job deleted");
              }}
            >
              Eliminar
            </Button>
            <Button
              className="btn-spaced"
              type="primary"
              size="large"
              onClick={() => {
                onUpdateJob(request);
                message.success("Job updated");
              }}
            >
              Editar
            </Button>
          </Fragment>
        ) : (
          <Button
            type="primary"
            size="large"
            onClick={() => {
              onSaveJob();
              message.success("Job saved");
            }}
          >
            Save
          </Button>
        )}
      </Row>
    </div>
  );
};

export default JobProfile;
