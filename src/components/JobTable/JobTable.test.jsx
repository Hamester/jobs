import React from 'react';
import ReactDOM from 'react-dom';
import JobTable from './JobTable';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<JobTable />, div);
    ReactDOM.unmountComponentAtNode(div);
});
