import React, { useEffect } from "react";
import { useJob } from "../../hooks/useJob";
import { message, Table } from "antd";
import { jobs, jobsColumns } from "../../mock/jobs";

import "./JobTable.less";

const JobTable = () => {
  const {
    setJobDashboard,
    setJobData,
    job: {
      dashboard: { dataSource },
    },
  } = useJob();

  useEffect(() => {
    if (!(Array.isArray(dataSource) && dataSource.length)) {
      setJobDashboard({ dataSource: jobs });
      message.success("New data has been loaded");
    }
  }, []);

  return (
    <Table
      rowKey="id"
      columns={jobsColumns}
      dataSource={dataSource}
      onRow={(row) => {
        return {
          onDoubleClick: () => {
            setJobData({
              displayRequest: true,
              request: { edit: true, ...row },
            });
          },
        };
      }}
      bordered
      scroll={{ x: "calc(100vw + 50%)" }}
      tableLayout="fixed"
    />
  );
};

export default JobTable;
