import React from "react";
import { Modal } from "antd";
import { useJob } from "../../hooks/useJob";

import JobProfile from "../JobProfile";

import "./JobModal.less";

const JobModal = () => {
  const {
    setJobData,
    clearJobRequest,
    job: { displayRequest },
  } = useJob();

  return (
    <Modal
      visible={displayRequest}
      onCancel={() => {
        setJobData({ displayRequest: false });
        clearJobRequest();
      }}
      footer={null}
    >
      <JobProfile />
    </Modal>
  );
};

export default JobModal;
