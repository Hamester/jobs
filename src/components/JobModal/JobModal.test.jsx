import React from 'react';
import ReactDOM from 'react-dom';
import JobModal from './JobModal';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<JobModal />, div);
    ReactDOM.unmountComponentAtNode(div);
});
