import { getRandom } from "../utils";
import { loremIpsum } from "lorem-ipsum";

const shortid = require("shortid");

const jobsTitle = [
  "Mother of Dragons, Khaleesi",
  "Hand of the King",
  "Lord Commander of the Night's Watch",
  "King in the North",
  "King of the Andals",
  "Maester",
  "Queen Regent",
  "Master of Whisperers",
  "Three Eye Raven",
  "Crow",
];

const companyNames = [
  "Dragonstone",
  "Stairs to Dragonstone",
  "King's Landing",
  "Winterfell",
  "Tower of Joy",
  "Beyond the Wall",
  "Highgarden",
  "Braavose",
  "Yunkai",
  "Quath",
  "Dorne",
  "Essos",
  "Casterly Rock",
  "Iron Islands",
  "Myr",
  "Volantis",
  "The Eyrie",
];

let jobs = [];
for (let index = 0; index < 5; index++) {
  const id = shortid.generate();
  const createdAt = new Date();
  const companyName = companyNames[getRandom(companyNames.length)];
  jobs.push({
    id,
    createdAt: createdAt.toDateString(),
    jobTitle: jobsTitle[getRandom(jobsTitle.length)],
    jobDescription: loremIpsum(),
    companyName,
    applyUrl: `www.${companyName
      .split(" ")
      .join("_")
      .toLocaleLowerCase()}.com`,
  });
}

export { jobs };

export const jobsColumns = [
  {
    title: "Date",
    dataIndex: "createdAt",
    width: 100,
    ellipsis: true,
  },
  {
    title: "Title",
    dataIndex: "jobTitle",
    width: 100,
    ellipsis: true,
  },
  {
    title: "Description",
    dataIndex: "jobDescription",
    width: 200,
    ellipsis: true,
  },
  {
    title: "Company Name",
    dataIndex: "companyName",
    width: 100,
    ellipsis: true,
  },
  {
    title: "Apply url",
    dataIndex: "applyUrl",
    width: 100,
    ellipsis: true,
  },
];
